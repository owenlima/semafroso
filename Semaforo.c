/******************************************************************************
*   arquivo...: Semaforo.c
*  
*
*
*   autor.....: Paulo José Ramos Almeida Modesto Owen Alves Lima     <paulo.jmodesto@sempreceub.com>  <owen.lima@sempreceub.com>
*   data......: 10/06/2019
*
*   OBS: Para executar no linux e: gcc -o Semaforo.o Semaforo.c
*
********************************************************************************/
//-- INCLUDE --------------------------------------------------------------------
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
//-- CONSTANTES -----------------------------------------------------------------
#define QUANT		(5)									//Quantidade de Filho
#define ESQUERDA	(id_filho + QUANT - 1) % QUANT	//Id do filho esquerda do id
#define DIREITA		(id_filho + 1) % QUANT			//Id do filho a direita do id
#define PENSANDO	(0)									//Id para estado pensado
#define FAMINTO		(1)									//Id para estado de fome
#define COMENDO		(2)									//Id para estado comendo
//-- GLOBAL ---------------------------------------------------------------------
int estado [QUANT];								//Estado dos filho
pthread_mutex_t mutex;							//Região crítica
pthread_mutex_t mux_filo [QUANT];				//Mutex por filho
pthread_t jantar[QUANT];						//Todos os filho
//-- PROTOTIPAÇÃO ---------------------------------------------------------------
void * filho ( void * param );
void pegar_hashi ( int id_filho );
void devolver_hashi ( int id_filho );
void intencao ( int id_filho );
void comer ( int id_filho );
void pensar ( int id_filho );
//-------------------------------------------------------------------------------
void * filho ( void * vparam )
{
	int * id = (int *) (vparam);	//Repassa o id do Filho

	printf("Filho %d foi criado com sucesso\n", *(id) );

	while ( 1 )
	{
		pensar( *(id) );			//Aguarda o filho pensar
		pegar_hashi( *(id) );		//Filho pega os hashis
		comer( *(id) );				//Aguarda comer
		devolver_hashi( *(id) );	//Devolver os hashis pra mesa
	}

	pthread_exit( (void*) 0 );		//Legado do retorno
}
//-------------------------------------------------------------------------------
void pegar_hashi ( int id_filho )
{
	pthread_mutex_lock( &(mutex) );					//Entra na região crítica
	printf("Filho %d esta faminto\n", id_filho);
	estado[ id_filho ] = FAMINTO;				//Altera o estado do filho
	intencao( id_filho );						//Intenção de pegar os hashis
	pthread_mutex_unlock( &(mutex) );				//Sai na região crítica
	pthread_mutex_lock( &(mux_filo[id_filho]) );	//Bloqueia os hashis
}
//-------------------------------------------------------------------------------
void devolver_hashi ( int id_filho)
{
	pthread_mutex_lock ( &(mutex) );	//Entra na região crítica
	printf("Filho %d esta pensando\n", id_filho);
	estado[ id_filho ] = PENSANDO;	//Terminou de comer
	intencao( ESQUERDA );				//Vê se o vizinho da esquerda pode comer agora
	intencao( DIREITA );				//Vê se o vizinho da direita pode comer agora
	pthread_mutex_unlock ( &(mutex) );	//Sai da região crítica
}
//---------------------------------------------------------------------------------------
void intencao ( int id_filho)
{
    printf("Filho %d esta com intencao de comer\n", id_filho);
    if ((estado[id_filho] == FAMINTO) && (estado[ESQUERDA] != COMENDO) && (estado[DIREITA] != COMENDO))
    {
        printf("Filho %d ganhou a vez de comer\n",id_filho);
        estado[id_filho] = COMENDO;     //Filho ganhou a vez de comer
        pthread_mutex_unlock(&(mux_filo[id_filho]));    //Liberar os hashis
    }
}
//---------------------------------------------------------------------------------------------
void pensar (int id_filho)
{
    int r = (rand() %10 +1);

    printf("Filho %d pensa por %d segundos\n",id_filho,r);
    sleep(r); //Ganha tempo em segundos
}
//------------------------------------------------------------------------------------------------------
void comer (int id_filho)
{
    int r = (rand() %10 +1);

    printf("Filho %d come por %d segundos\n",id_filho,r);
    sleep(r); //Gasta um tempo por segundos
}
//-------------------------------------------------------------------------------------------------------
int main (void)
{
    int cont;   //Contador auxiliar
    int status;  //Criação de thread

    //Inicia os mutexes
    pthread_mutex_init(&(mutex), NULL);
    for(cont = 0; cont < QUANT; cont++)
    {
        pthread_mutex_init(&(mux_filo[cont]), NULL);
    }

    //Inicia thread (Filho)
    for (cont = 0; cont < QUANT; cont++)
    {
        status = pthread_create( &(jantar[cont]),NULL, filho, (void *) &(cont));
        if(status)
        {
            printf("Erro criando thread %d, retorno do codigo %d\n",cont,status);
            return EXIT_FAILURE;
        }

        //Destroi antes de sair
        pthread_mutex_destroy( &(mutex));
        for ( cont = 0; cont < QUANT; cont++)
        {
            pthread_mutex_destroy( &(mux_filo[cont]));
        }
        pthread_exit(NULL);

        return EXIT_SUCCESS;
        
    }
}
